package com.findifyapp.utils.rss;


import com.findifyapp.exception.RssDateParseException;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public final class RssDateParser {
    private static final String RSS_DATE_FORMAT_WITH_GENERAL_TIMEZONE_PATTERN = "EEE, dd MMM yyyy HH:mm:ss zzz";
    private static final String RSS_DATE_FORMAT_WITH_RFC_822_TIMEZONE_PATTERN = "EEE, dd MMM yyyy HH:mm:ss Z";
    private static final String RSS_DATE_FORMAT_WITHOUT_TIMEZONE_PATTERN = "EEE, dd MMM yyyy HH:mm:ss";

    private static final String[] RSS_PATTERNS_WITH_TIME_ZONE = {
            "EEE, dd MMM yyyy HH:mm:ss zzz",
            "EEE, dd MMM yyyy HH:mm:ss Z",
            "EEE, dd MMM yy HH:mm:ss zzz",
            "EEE, dd MMM yy HH:mm:ss Z"
    };

    private static final String[] RSS_PATTERNS_WITHOUT_TIME_ZONE = {
            "EEE, dd MMM yy HH:mm:ss",
            "EEE, dd MMM yyyy HH:mm:ss"
    };

    private static final String[] ANOTHER_PATTERNS_WITHOUT_TIME_ZONE = {//add here pattern to extend parsing logic but only without timezone
            "yyyy-MM-dd HH:mm:ss",
            "EEE, dd MMM yyyy k:mm:ss"
    };

    private static final String[] ANOTHER_FORMATS_WITH_TIME_ZONE = {//add here pattern to extend parsing logic but only with timezone
            "yyyy-MM-dd HH:mm:ss",
            "EEE, dd MMM yyyy k:mm:ss"
    };

    private static DateTimeFormatter rssDateGeneralTimeZoneFormatter = DateTimeFormatter.ofPattern(RSS_DATE_FORMAT_WITH_GENERAL_TIMEZONE_PATTERN);
    private static DateTimeFormatter rssDateRfcTimeZoneFormatter = DateTimeFormatter.ofPattern(RSS_DATE_FORMAT_WITH_RFC_822_TIMEZONE_PATTERN);
    private static DateTimeFormatter rssWithoutTimeZoneDateFormatter = DateTimeFormatter.ofPattern(RSS_DATE_FORMAT_WITHOUT_TIMEZONE_PATTERN);

    public static ZonedDateTime parsePublicationDateToZoneDate(String stringDate) throws RssDateParseException {
        ZonedDateTime zonedDateTime;

        try {
            zonedDateTime = parsePublicationDateWithTimeZone(stringDate);
        } catch (RssDateParseException e1) {
            LocalDateTime localDateTime = parsePublicationDateWithoutTimeZone(stringDate);
            zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        }

        return zonedDateTime;
    }

    public static LocalDateTime parsePublicationDateToLocalDateTime(String stringDate) throws RssDateParseException {
        LocalDateTime localDateTime;
        try {
            localDateTime = parsePublicationDateToZoneDate(stringDate).toLocalDateTime();
        } catch (RssDateParseException e1) {
            localDateTime = parsePublicationDateWithoutTimeZone(stringDate);
        }
        return localDateTime;
    }

    private static ZonedDateTime parsePublicationDateWithTimeZone(String stringDate) throws RssDateParseException {
        ZonedDateTime zonedDateTime = null;

        for (String pattern : RSS_PATTERNS_WITH_TIME_ZONE) {
            if (zonedDateTime == null) {
                zonedDateTime = parseToZonedDateTime(stringDate, DateTimeFormatter.ofPattern(pattern));
            } else {
                break;
            }
        }

        if (zonedDateTime == null) {
            for (String pattern : ANOTHER_FORMATS_WITH_TIME_ZONE) {
                if (zonedDateTime == null) {
                    zonedDateTime = parseToZonedDateTime(stringDate, DateTimeFormatter.ofPattern(pattern));
                } else {
                    break;
                }
            }
        }

        if (zonedDateTime == null) {
            throw new RssDateParseException(
                    "Can't parse date : " + stringDate + " to ZonedTime ");
        }
        return zonedDateTime;
    }

    public static LocalDateTime parsePublicationDateWithoutTimeZone(String stringDate) throws RssDateParseException {
        LocalDateTime localDateTime = null;

        for (String pattern : RSS_PATTERNS_WITHOUT_TIME_ZONE) {
            if (localDateTime == null) {
                localDateTime = parseToLocalDateTime(stringDate, DateTimeFormatter.ofPattern(pattern));
            } else {
                break;
            }
        }

        if (localDateTime == null) {
            for (String pattern : ANOTHER_PATTERNS_WITHOUT_TIME_ZONE) {
                if (localDateTime == null) {
                    localDateTime = parseToLocalDateTime(stringDate, DateTimeFormatter.ofPattern(pattern));
                } else {
                    break;
                }
            }
        }
        if (localDateTime == null) {
            throw new RssDateParseException(
                    "Can't parse date : " + stringDate + " to LocalDateTime ");
        }

        return localDateTime;
    }


    @Nullable
    static ZonedDateTime parseToZonedDateTime(String stringDate, DateTimeFormatter formatter) {
        ZonedDateTime zonedDateTime;
        try {
            zonedDateTime = ZonedDateTime.parse(stringDate, formatter);
        } catch (DateTimeParseException e) {
            return null;
        }
        return zonedDateTime;
    }

    @Nullable
    static LocalDateTime parseToLocalDateTime(String stringDate, DateTimeFormatter formatter) {
        LocalDateTime localDateTime;
        try {
            localDateTime = LocalDateTime.parse(stringDate, formatter);
        } catch (DateTimeParseException e) {
            return null;
        }
        return localDateTime;
    }

    public static String getAsPublicationDateWithoutTimeZone(LocalDateTime localDateTime) {
        return rssWithoutTimeZoneDateFormatter.format(localDateTime);
    }

    public static String getAsPublicationDateWithRfc822TimeZone(ZonedDateTime zonedDateTime) {
        return rssDateRfcTimeZoneFormatter.format(zonedDateTime);
    }

    public static String getAsPublicationWithGeneralTimeZone(ZonedDateTime zonedDateTime) {
        return rssDateGeneralTimeZoneFormatter.format(zonedDateTime);
    }
}