package com.findifyapp.controller;

import com.findifyapp.dto.rss.Rss;
import com.findifyapp.service.rss.RssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rss")
public class RssController {

    @Autowired
    RssService rssService;

    @GetMapping(produces = MediaType.APPLICATION_XML_VALUE)
    public Rss getRssByUrl(@RequestParam("url") String url) {
    return rssService.getRssByUrl(url);
    }
}