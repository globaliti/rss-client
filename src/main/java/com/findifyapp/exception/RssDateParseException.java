package com.findifyapp.exception;

public class RssDateParseException extends Exception {
    public RssDateParseException() {
        super();
    }

    public RssDateParseException(String message) {
        super(message);
    }

    public RssDateParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public RssDateParseException(Throwable cause) {
        super(cause);
    }

    protected RssDateParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
