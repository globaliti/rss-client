package com.findifyapp.exception;

public class RssDateFormattingException extends Exception {
    public RssDateFormattingException() {
        super();
    }

    public RssDateFormattingException(String message) {
        super(message);
    }

    public RssDateFormattingException(String message, Throwable cause) {
        super(message, cause);
    }

    public RssDateFormattingException(Throwable cause) {
        super(cause);
    }

    protected RssDateFormattingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
