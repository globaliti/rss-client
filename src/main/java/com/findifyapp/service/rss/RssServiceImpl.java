package com.findifyapp.service.rss;

import com.findifyapp.dto.rss.Rss;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.Collections;

@Service
public class RssServiceImpl implements RssService {

    private static final Logger log = Logger.getLogger(RssServiceImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    public RssServiceImpl() {
    }

    @Nullable
    @Override
    public Rss getRssByUrl(String url) {
        Rss rss = loadFromXmlFormat(url);
        if (rss == null) {
            rss = loadFromTextHtmlFormat(url);
        }
        return rss;
    }

    @Nullable
    private Rss loadFromXmlFormat(String url) {
        HttpEntity<String> entity = new HttpEntity<>(null, getHeadersForRssRequest());
        ResponseEntity<Rss> rssResponseEntity = null;

        try {
            rssResponseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, Rss.class);
        } catch (RuntimeException runtimeException) {
            log.error("Exception while getting  rss from : " + url + " exception:" + runtimeException.getMessage());
        }

        if (rssResponseEntity != null && rssResponseEntity.getStatusCode().is3xxRedirection()) {
            return loadFromXmlFormat(rssResponseEntity.getHeaders().getLocation().toString());
        }

        if (rssResponseEntity != null && rssResponseEntity.getStatusCode().is2xxSuccessful()) {
            return rssResponseEntity.getBody();
        }
        return null;
    }

    @Nullable
    private Rss loadFromTextHtmlFormat(String url) {
        HttpEntity<String> entity = new HttpEntity<>(null, getHeadersForRssRequestAsTextHtml());
        ResponseEntity<String> rssResponseStringEntity = null;

        try {
            rssResponseStringEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        } catch (RuntimeException runtimeException) {
            log.error("Exception while getting  rss from : " + url + " exception:" + runtimeException.getMessage());
        }

        if (rssResponseStringEntity != null && rssResponseStringEntity.getStatusCode().is2xxSuccessful()) {
            String xmlDocument = rssResponseStringEntity.getBody();
            JAXBContext jaxbContext;
            try {
                jaxbContext = JAXBContext.newInstance(Rss.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                Object marshResult = jaxbUnmarshaller.unmarshal(new StringReader(xmlDocument));
                if (marshResult instanceof Rss) {
                    return (Rss) marshResult;
                }

            } catch (JAXBException e) {
                e.printStackTrace();
            }

        }
        if (rssResponseStringEntity != null && rssResponseStringEntity.getStatusCode().is3xxRedirection()) {
            return loadFromTextHtmlFormat(rssResponseStringEntity.getHeaders().getLocation().toString());
        }
        return null;
    }

    private HttpHeaders getHeadersForRssRequest() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        return headers;
    }

    private HttpHeaders getHeadersForRssRequestAsTextHtml() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.TEXT_HTML));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        return headers;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
