package com.findifyapp.service.rss;


import com.findifyapp.dto.rss.Rss;
import org.springframework.lang.Nullable;

public interface RssService {

    @Nullable
    Rss getRssByUrl(String url);
}
