package com.findifyapp.dto.rss;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
public class Atom {
    @XmlAttribute
    private String rel;

    @XmlAttribute
    private String href;

    @XmlAttribute
    private String type;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Atom atom = (Atom) o;
        return Objects.equals(rel, atom.rel) &&
                Objects.equals(href, atom.href) &&
                Objects.equals(type, atom.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rel, href, type);
    }

    @Override
    public String toString() {
        return "Atom{" +
                "rel='" + rel + '\'' +
                ", href='" + href + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}