package com.findifyapp.dto.rss;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
public class Item {
    @XmlElement
    private String link;

    @XmlElement
    private String guid;

    @XmlElement
    private String description;

    @XmlElement
    private String title;

    @XmlElement
    private String category;

    @XmlElement
    private String pubDate;

    @XmlElement
    private String author;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(link, item.link) &&
                Objects.equals(guid, item.guid) &&
                Objects.equals(description, item.description) &&
                Objects.equals(title, item.title) &&
                Objects.equals(category, item.category) &&
                Objects.equals(pubDate, item.pubDate) &&
                Objects.equals(author, item.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(link, guid, description, title, category, pubDate, author);
    }

    @Override
    public String toString() {
        return "Item{" +
                "link='" + link + '\'' +
                ", guid='" + guid + '\'' +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", category='" + category + '\'' +
                ", pubDate='" + pubDate + '\'' +
                ", author='" + author + '\'' +
                '}';
    }
}
