package com.findifyapp.dto.rss;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
public class Channel {
    @XmlElement
    private List<Item> item;

    @XmlElement
    private String link;

    @XmlElement
    private String description;

    @XmlElement
    private String language;

    @XmlElement
    private String title;

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Channel channel = (Channel) o;
        return Objects.equals(item, channel.item) &&
                Objects.equals(link, channel.link) &&
                Objects.equals(description, channel.description) &&
                Objects.equals(language, channel.language) &&
                Objects.equals(title, channel.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item, link, description, language, title);
    }

    @Override
    public String toString() {
        return "Channel{" +
                "item=" + item +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", language='" + language + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
