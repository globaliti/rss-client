package com.findifyapp.utils.rss;

import com.findifyapp.exception.RssDateParseException;
import junit.framework.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class RssDateParserTest {

    @Test
    public void parsePublicationDateToLocalDateTime() throws RssDateParseException {

        LocalDateTime expectDate;
        LocalDateTime parsedDate;

        expectDate = LocalDateTime.of(2019, 11, 13, 4, 25, 48);
        parsedDate = RssDateParser.parsePublicationDateToLocalDateTime("Wed, 13 Nov 2019 04:25:48 GMT");

        Assert.assertEquals(expectDate, parsedDate);

        parsedDate = RssDateParser.parsePublicationDateToLocalDateTime("Wed, 13 Nov 2019 04:25:48 EST");
        Assert.assertEquals(expectDate, parsedDate);

        parsedDate = RssDateParser.parsePublicationDateToLocalDateTime("Wed, 13 Nov 2019 04:25:48 +0000");
        Assert.assertEquals(expectDate, parsedDate);

        parsedDate = RssDateParser.parsePublicationDateToLocalDateTime("Wed, 13 Nov 2019 04:25:48");
        Assert.assertEquals(expectDate, parsedDate);

        parsedDate = RssDateParser.parsePublicationDateToLocalDateTime("Wed, 13 Nov 19 04:25:48");
        Assert.assertEquals(expectDate, parsedDate);

        parsedDate = RssDateParser.parsePublicationDateToLocalDateTime("Wed, 13 Nov 19 04:25:48 +0000");
        Assert.assertEquals(expectDate, parsedDate);

        parsedDate = RssDateParser.parsePublicationDateToLocalDateTime("Wed, 13 Nov 19 04:25:48 GMT");
        Assert.assertEquals(expectDate, parsedDate);

        parsedDate = RssDateParser.parsePublicationDateToLocalDateTime("2019-11-13 04:25:48");
        Assert.assertEquals(expectDate, parsedDate);

        parsedDate = RssDateParser.parsePublicationDateToLocalDateTime("Wed, 13 Nov 2019 4:25:48");
        Assert.assertEquals(expectDate, parsedDate);
    }

    @Test
    public void getAsPublicationDate() {
        LocalDateTime date = LocalDateTime.of(2019, 11, 13, 4, 25, 48);
        org.junit.Assert.assertEquals("Wed, 13 Nov 2019 04:25:48",RssDateParser.getAsPublicationDateWithoutTimeZone(date));
        org.junit.Assert.assertEquals("Wed, 13 Nov 2019 04:25:48 +0000",RssDateParser.getAsPublicationDateWithRfc822TimeZone(ZonedDateTime.of(date, ZoneId.of("GMT"))));
        org.junit.Assert.assertEquals("Wed, 13 Nov 2019 04:25:48 GMT",RssDateParser.getAsPublicationWithGeneralTimeZone(ZonedDateTime.of(date, ZoneId.of("GMT"))));
    }
}